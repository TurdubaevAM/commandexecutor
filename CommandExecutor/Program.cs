﻿using CommandExecutor.Commands;
using CommandExecutor.EventArguments;
using System;
using System.Threading.Tasks;

namespace CommandExecutor
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // let's create a couple of commands and subscribers
            var tempCommand = new TemperatureDeviceCommand();
            tempCommand.ExecutionComplited += TempCommand_ExecutionComplited;

            var exceptionCommand = new SomeLogicalCommandThatThrowException() { Commands = new ICommand[] { new EmptyCommand(), new FailCommand() } };
            exceptionCommand.ExecutionComplited += ExceptionCommand_ExecutionComplited;

            var commands = new ICommand[]
            {
                new RadarDeviceCommand(),
                tempCommand,
                exceptionCommand,
                new AnalyzeSerialPortLogicalCommand() { Commands = new ICommand[] { new EmptyCommand(), new RadarDeviceCommand()} },
            };

            var dispatcher = new CommandDispatcher(commands);

            await dispatcher.TryExecuteCommand(new TemperatureDeviceCommand());
        }

        private static void ExceptionCommand_ExecutionComplited(object sender, EventArgs e)
        {
            Console.WriteLine("\t\t Client has received a notification from some of logical commans which threw an exception. Operation Status = {0}",
                (e as SomeLogicalCommandThatThrowExceptionEventArgs).Succeed);
        }

        private static void TempCommand_ExecutionComplited(object sender, EventArgs e)
        {
            Console.WriteLine("\t\t Client has received a notification about temperature. Current temperature = {0}",
                (e as TemperatureCommandEventArgs).Temperature);
        }
    }
}
