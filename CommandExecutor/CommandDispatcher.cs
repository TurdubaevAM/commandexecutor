﻿using CommandExecutor.Commands;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CommandExecutor
{
    class CommandDispatcher
    {
        private readonly Queue<ICommand> _commandsQueue;

        public CommandDispatcher(ICommand[] commands)
        {
            if (commands is null)
                throw new ArgumentNullException(nameof(commands));

            _commandsQueue = new Queue<ICommand>();

            foreach (var cmd in commands)
            {
                _commandsQueue.Enqueue(cmd);
            }
        }

        public async Task TryExecuteCommand(ICommand cmd)
        {
            if (cmd is null)
                throw new ArgumentNullException(nameof(cmd));

            _commandsQueue.Enqueue(cmd);


            while (_commandsQueue.TryDequeue(out ICommand nextCommand))
            {
                await nextCommand.Execute();
            }
        }
    }
}
