﻿using System;

namespace CommandExecutor.EventArguments
{
    class SomeLogicalCommandThatThrowExceptionEventArgs : EventArgs
    {
        public bool Succeed { get; set; }
        public string SomeData { get; set; }
    }
}
