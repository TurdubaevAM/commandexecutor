﻿using System;

namespace CommandExecutor.EventArguments
{
    class SerialPortAnaliseEventArgs : EventArgs
    {
        public TimeSpan TimeElapsed { get; set; }
        public string SerialPortOutput { get; set; }
    }
}
