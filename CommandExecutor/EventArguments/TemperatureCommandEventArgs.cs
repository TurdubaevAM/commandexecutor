﻿using System;

namespace CommandExecutor.EventArguments
{
    class TemperatureCommandEventArgs : EventArgs
    {
        public float Temperature { get; set; }
    }
}
