﻿using System;

namespace CommandExecutor.EventArguments
{
    class RadarDeviceEventArgs : EventArgs
    {
        public int Distance { get; set; }
    }
}
