﻿namespace CommandExecutor.Commands
{
    interface ILogicalCommand : ICommand
    {
        public ICommand[] Commands { get; set; }
    }
}
