﻿using CommandExecutor.EventArguments;
using System;
using System.Threading.Tasks;

namespace CommandExecutor.Commands
{
    class TemperatureDeviceCommand : CommandBase
    {
        public override async Task Execute()
        {
            Console.WriteLine($"Getting data from Temperature sensor");

            // some long running action
            await Task.Delay(2000);

            OnExecutionComplited(new TemperatureCommandEventArgs { Temperature = 36.6f });
        }
    }
}
