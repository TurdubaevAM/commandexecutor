﻿using CommandExecutor.EventArguments;
using System;
using System.Threading.Tasks;

namespace CommandExecutor.Commands
{
    class RadarDeviceCommand : CommandBase
    {
        public override async Task Execute()
        {
            Console.WriteLine($"Getting data from Radar sensor");

            // some long running action
            await Task.Delay(2000);

            OnExecutionComplited(new RadarDeviceEventArgs { Distance = 2000 });
        }
    }
}
