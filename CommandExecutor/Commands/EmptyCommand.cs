﻿using System;
using System.Threading.Tasks;

namespace CommandExecutor.Commands
{
    class EmptyCommand : CommandBase
    {
        public async override Task Execute()
        {
            Console.WriteLine($"Executing empty command ");

            OnExecutionComplited(new EventArgs());

            await Task.CompletedTask;
        }
    }
}
