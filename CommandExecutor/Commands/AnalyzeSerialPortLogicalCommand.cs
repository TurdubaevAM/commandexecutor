﻿using CommandExecutor.EventArguments;
using System;
using System.Threading.Tasks;

namespace CommandExecutor.Commands
{
    class AnalyzeSerialPortLogicalCommand : CommandBase, ILogicalCommand
    {
        public ICommand[] Commands { get; set; }

        public AnalyzeSerialPortLogicalCommand()
        {
            // initialize your command sequence here or pass it to the constructor
            // or just pass it via initialization syntax as I did in the Main() method
        }

        public override async Task Execute()
        {
            // executing some long running commands sequence
            for (var idx = 0; idx < Commands.Length; idx++)
            {
                Console.WriteLine($"Exeuting command #{ idx } from AnalyzeSerialPortLogicalCommand list");
                await Task.Delay(2000);

                await Commands[idx].Execute();
            }

            var args = new SerialPortAnaliseEventArgs { SerialPortOutput = "The execution of the command completed", TimeElapsed = new TimeSpan(123) };

            OnExecutionComplited(args);
        }
    }
}
