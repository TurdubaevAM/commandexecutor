﻿using System;
using System.Threading.Tasks;

namespace CommandExecutor.Commands
{
    class FailCommand : CommandBase
    {
        public async override Task Execute()
        {
            await Task.Delay(1000);
            
            Console.WriteLine($"Wait a second, and I will throw an exception for you =) ");

            throw new NotImplementedException("It's sad but this method should throw an exception");
        }
    }
}
