﻿using System;
using System.Threading.Tasks;

namespace CommandExecutor.Commands
{
    abstract class CommandBase : ICommand
    {
        public event EventHandler ExecutionComplited;

        public abstract Task Execute();

        protected void OnExecutionComplited(EventArgs e)
        {
            var handler = ExecutionComplited;

            handler?.Invoke(this, e);
        }
    }
}
