﻿using CommandExecutor.EventArguments;
using System;
using System.Threading.Tasks;

namespace CommandExecutor.Commands
{
    class SomeLogicalCommandThatThrowException : CommandBase, ILogicalCommand
    {
        public ICommand[] Commands { get; set; } = new ICommand[] { };

        public SomeLogicalCommandThatThrowException()
        {
            // initialize your command sequence here or pass it to the constructor
            // or just pass it via initialization syntax as I did in the Main() method
        }

        public override async Task Execute()
        {
            // executing some long running commands sequence
            // one of these internal commands should throw an exception during the invocation

            try
            {

                for (var idx = 0; idx < Commands.Length; idx++)
                {
                    Console.WriteLine($"Exeuting command #{ idx } from SomeLogicalCommandThatThrowException list");
                    await Task.Delay(2000);

                    await Commands[idx].Execute();
                }

                OnExecutionComplited(new SomeLogicalCommandThatThrowExceptionEventArgs() { Succeed = true });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // It would be better to wrap this exception handling somehow to make this code better
                // To notify clients code about unsuccessful execution use EventArgs as I did in other places, e.g.
                OnExecutionComplited(new SomeLogicalCommandThatThrowExceptionEventArgs() { Succeed = false });
            }
        }
    }
}
