﻿using System;
using System.Threading.Tasks;

namespace CommandExecutor.Commands
{
    interface ICommand
    {
        event EventHandler ExecutionComplited;

        Task Execute();
    }
}
